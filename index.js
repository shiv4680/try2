const { request } = require("express");
const express = require("express");
const cors = require("cors")
const User = require("./db/User");
const Product = require("./db/product")
const Jwt = require('jsonwebtoken');
const JwtKey = 'e-comm';
const app = express();

app.use(express.json());   //  both are used as middelware
app.use(cors());

const mongoose = require('mongoose');
const product = require("./db/product");
mongoose.connect('mongodb://localhost:27017/E-commerce', { useNewUrlParser: true });
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
    console.log("Database connected");
});

app.get("/user", async (req, resp) => {
    console.log("user id : ", req.query.id);
    let user = await User.findOne({ "_id": req.query.id });
    console.log("debug line 21 : ", user);
    resp.send(user)
})

app.post("/register", async (req, resp) => {
    console.log("Inside register() | req.body : ", req.body);
    let user = new User(req.body);
    let result = await user.save();
    result = result.toObject();
    delete result.password

    if (req.body.password.length < 8) {
        return resp.status(404).json({ "message": "password length must be greater than 7 characters" });
    }
    Jwt.sign({ result }, JwtKey, { expiresIn: "2h" }, (err, token) => {
        if (err) {
            resp.send(err);
        }
        resp.send({
            result, auth: token
        });
    })
})

// app.post("/login",async (req,resp)=>{
//     let user= await User.findOne(req.body).select("-password");
//     if(user)
//     {
//         resp.send(user)
//     }else{
//         resp.send({result:'No user found'})
//     }
//     resp.send(user);

// })

app.post("/login", async (req, resp) => {
    console.log("login : ", req.body);
    let { email, password } = req.body;

    if (!email || !password) {
        resp.send({ result: 'Email or password not entered' })
    }

    //Alternative: For removing password from the user response
    // let user= await User.findOne({"email":email,"password":password}).select("-password");

    let user = await User.findOne({ "email": email, "password": password });
    if (!user) {
        resp.send({ result: 'No user found. Email or password is incorrect' });
        // - find user with email id
        //user= await User.findOne({"email":email})
        //- send a mail to that email id saying incorrect login attempt
    } //jwt vtoken generation code - for authentication -security concern 
    Jwt.sign({ user }, JwtKey, { expiresIn: "2h" }, (err, token) => {
        if (err) {
            resp.send(err);
        }
        resp.send({
            user: {
                _id: user._id,
                name: user.name
            },
            auth: token
        });
    })

})

app.post("/add-product", verifyToken ,async (req, resp) => {
    let product = new Product(req.body);
    let result = await product.save();
    resp.send(result);
});

app.get("/products",verifyToken , async (req, resp) => {
    let products = await product.find();
    console.log("debug-1 : ", products);
    if (products.length > 0) {
        resp.send(products)
    } else {
        resp.send({ result: "no product found " })
    }
})

app.delete("/product/:id",verifyToken , async (req, resp) => {
    const result = await Product.deleteOne({ _id: req.params.id })
    resp.send(result);

});
app.get("/product/:id", verifyToken ,async (req, resp) => {
    let result = await product.findOne({ _id: req.params.id })
    if (result) {
        resp.send(result);
    } else {
        resp.send({ result: "No record found" })
    }
});

app.put("/product/:id", verifyToken ,async (req, resp) => {
    let result = await Product.updateOne(
        { _id: req.params.id },
        { $set: req.body }
    )
    resp.send(result)

})

app.get("/search/:key", verifyToken, async (req, resp) => {
    let result = await product.find({
        "$or": [
            { name: { $regex: req.params.key } },
            { company: { $regex: req.params.key } },
            { category: { $regex: req.params.key } }

        ] //to search more than one field in object we use "$or"
    });
    resp.send(result)
})
function verifyToken(req, resp, next) {
    let token = req.headers['authorization'];
    if (token) {
        token = token.split(' ')[1];
        console.warn("middleware called if : ", token)
        Jwt.verify(token, JwtKey, (err, valid) => {
            if (err) {
                resp.status(401).send({ result: "please provide valid token" })

            } else {
                next();
            }
        })
    } else {
        resp.status(401).send({ result: "please add token with header" })
    }
    //console.warn("middlleware called token",token);
}


app.listen(4000, () => {
    console.log("server started");
})